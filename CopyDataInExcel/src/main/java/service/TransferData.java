package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.XMLConstants;
import util.Utils;

public class TransferData {
	private FileOutputStream outFile;
	private static Logger LOG = Logger.getLogger(TransferData.class);
	public static Map<String, String> xmlConfig;
	
	static {
		xmlConfig = loadXMLConfig();
	}
	
	public void copyAll(final XSSFWorkbook workbook, final String srcFileName) {
		LOG.debug("---Copy all data from excel file - BEGIN ");
		final String pathOutput = xmlConfig.get(XMLConstants.WORKSPACE_OUTPUT);

		try {
			final String desFileName = Utils.formatName(srcFileName);
			if(Utils.checkValue(desFileName)) {
				outFile = new FileOutputStream(new File(pathOutput+desFileName));
				workbook.write(outFile);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			LOG.error("File Not Found Exception" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.error("IO Exception " + e.getMessage());
			e.printStackTrace();
		}
		LOG.debug("---Copy all data from excel file - END ");
	}

	public static Map<String, String> loadXMLConfig() {
		LOG.debug("---Load XML configuration - BEGIN ");
		Map<String, String> mapConfig = new HashMap<String, String>();
		Properties properties = new Properties();
		try {
			InputStream input = new FileInputStream(XMLConstants.CONFIG_PATH);
			properties.loadFromXML(input);
			Utils.converXMLToElements(properties, mapConfig);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LOG.debug("---Load XML configuration - END ");
		return mapConfig;
	}

	/*	choose name will copy
	 * while(exit){choose} -> copy */
	public void createEachSheet(final XSSFWorkbook workbook, final String srcFileName, final XSSFSheet sheet) {
		LOG.info("---Create new sheet - BEGIN");
		
		LOG.info("***Enter name of destination sheet:");
		Scanner sc = new Scanner(System.in);
		final String newSheetName = sc.nextLine();
		final XSSFSheet sheetDes = workbook.createSheet(newSheetName);
		
		//
		Iterator<Row> listRow = sheet.iterator();
		int rowNum = 0;
		while(listRow.hasNext()) {
			//read source each row
			final Row row = listRow.next();
			//create des each row 
			final Row rowDes = sheetDes.createRow(rowNum);
			int cellNum = 0;
			Iterator<Cell> listCell = row.cellIterator();
			while(listCell.hasNext()) {
				//read source each cell
				final Cell cell = listCell.next();
				//create and set des each cell
				final CellType cellType = cell.getCellType();
				Cell cellDes ;
				switch(cellType) {
				case NUMERIC:
					cellDes = rowDes.createCell(cellNum, cellType);
					cellDes.setCellValue(cell.getNumericCellValue());
					break;
				case STRING:
					cellDes = rowDes.createCell(cellNum, cellType);
					cellDes.setCellValue(cell.getStringCellValue());
					break;
				}
				cellNum++;
			}
			rowNum++;
		}
		
		LOG.info("---Create new sheet - END");
	}
}
