package model;

public final class Constants {
	public static final String STRING_ZERO = "0";
	public static final String STRING_ONE = "1";
	public static final String STRING_TWO = "2";
	public static final String STRING_THREE = "3";
	public static final String STRING_FOUR = "4";
	public static final String STRING_FIVE = "5";
	
	public static final int INT_ZERO = 0;
	public static final int INT_ONE = 1;
	public static final int INT_TWO = 2;
	public static final int INT_THREE = 3;
	public static final int INT_FOUR = 4;
	public static final int INT_FIVE = 5;
	
	public static final String STRING_COPY_ALL = "Copy all data the same this file";
	public static final String STRING_COPY_EACH_SHEET = "Copy each sheet";
	public static final String STRING_COPY_CUSOMIZE = "Copy with customize";
	
	public static final String STRING_EMPTY = "";
	public static final String STRING_DOT = ".";
	
	
}
