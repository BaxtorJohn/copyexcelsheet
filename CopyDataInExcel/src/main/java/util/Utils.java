package util;

import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import model.Constants;
import model.XMLConstants;

public final class Utils {
	public static final Logger LOG = Logger.getLogger(Utils.class);
	private static final String STRING_COPY = "(Copy)";

	public static int converToInt(String str) {
		int result = 0;
		if (str != null || str != Constants.STRING_EMPTY) {
			String temp = str.replaceAll("-", "");
			try {
				result = Integer.parseInt(temp);
			} catch (NumberFormatException nf) {
				LOG.error("Can't parse String to integer:" + nf);
			}
		}
		return result;
	}

	public static void converXMLToElements(Properties properties, Map<String, String> mapConfig) {
		mapConfig.put(XMLConstants.WORKSPACE_OUTPUT, properties.getProperty(XMLConstants.WORKSPACE_OUTPUT));
	}

	public static String formatName(String srcFileName) {
		final StringBuilder result = new StringBuilder();
		StringBuilder fileName = new StringBuilder();
		String extension;
		final StringTokenizer strToken = new StringTokenizer(srcFileName, Constants.STRING_DOT);

		fileName.append(strToken.nextToken());
		if (!strToken.hasMoreTokens()) {
			extension = fileName.toString();
			fileName = new StringBuilder();
		} else {
			extension = strToken.nextToken();
			if (strToken.hasMoreElements()) {
				fileName.append(Constants.STRING_DOT).append(extension);
				extension = strToken.nextToken(); // not validate what extension?
			}
		}
		fileName.append(STRING_COPY);
		result.append(fileName).append(Constants.STRING_DOT).append(extension);
		return result.toString();
	}

	public static boolean checkValue(final String value) {
		if (value.isEmpty() || value == null) {
			return false;
		}
		return true;
	}
}
