package userinterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.Constants;
import model.XMLConstants;
import service.TransferData;
import util.Utils;

public class Console extends Thread {
	public static String pathFile;
	private static Map<String, String> conditions;
	private static Scanner sc = new Scanner(System.in);
	private static XSSFWorkbook workbook = null;
	// private static HSSFWorkbook workbook;
	private static Logger LOG = Logger.getLogger(Console.class);
	private static final String STRING_EXIT = "exit";
	private static final String STRING_QUIT = "quit";
	private static final String STRING_COPY = "copy";
	static {
		conditions = new HashMap<String, String>();
		conditions.put(Constants.STRING_ONE, Constants.STRING_COPY_ALL);
		conditions.put(Constants.STRING_TWO, Constants.STRING_COPY_EACH_SHEET);
		conditions.put(Constants.STRING_THREE, Constants.STRING_COPY_CUSOMIZE);
	}

	public Console() {
		LOG.info("==> Init Pathfile - BEGIN");
		System.out.println(
				"Welcome to Copy Data in Excel to another program!" + "\nPlease enter your absolute path file: ");
		pathFile = sc.nextLine();
		LOG.info("==> Init Pathfile - END");
	}

	/**
	 * creates an {@link HSSFWorkbook} with the specified OS filename.
	 */
	private void readFile(final File filename) throws IOException {
		try (FileInputStream fis = new FileInputStream(filename)) {
			workbook = new XSSFWorkbook(fis);
//			return workbook;        // NOSONAR - should not be closed here
		}
	}

	public void run() {
		LOG.debug("==> Run - Begin");
		displayConditions();
		String line;
		TransferData service = new TransferData();
		final File fileSrc = new File(pathFile);
		try {
			readFile(fileSrc);
		} catch (FileNotFoundException e) {
			LOG.debug("File Not Found Exception" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.debug("IO Exception " + e.getMessage());
			e.printStackTrace();
		}

		line = sc.nextLine();
		Integer key = Utils.converToInt(line);
		switch (key) {
		case 1:
			service.copyAll(workbook, fileSrc.getName());
		
		case 2: // (A) Copy not the same name in 1 xlsx file
				final XSSFWorkbook outWorkbook = new XSSFWorkbook();
				displayNameSheet();
				
//				String input;
				String sheetName;
				while (true) {
				LOG.info(
						"***Enter your name of sheet:\t" + "-\tEnter \"copy\" if you want to copy with sheet chosen.");
				sheetName = sc.nextLine();
				
				//if user choose option copy
				if(Utils.checkValue(sheetName) && sheetName.equals(STRING_COPY)) {
					//check workbook not empty
					outWorkbook.getNumberOfSheets();
					break;
				}
				
				while (true) {
					if (Utils.checkValue(sheetName)) {
						if (sheetName.equalsIgnoreCase(STRING_QUIT)) {
							break;
						}

						XSSFSheet sourceSheet = workbook.getSheet(sheetName);
						// create sheet in out workbook and write data into it.
						service.createEachSheet(outWorkbook, fileSrc.getName(), sourceSheet);
					} else {
						System.out.println("[ERROR]: " + sheetName + " not valid. Enter again!");
						LOG.debug(sheetName + " not valid. Enter again!");
					}
					// (A) copy
					LOG.info("***Enter name destination file:");// adjust bussiness after
					final String desFileName = sc.nextLine();
					File desFile = new File(
							TransferData.xmlConfig.get(XMLConstants.WORKSPACE_OUTPUT) + File.separator + desFileName);
					try {
						FileOutputStream out = new FileOutputStream(desFile);
						outWorkbook.write(out);
						outWorkbook.close();
						out.close();

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		case 3: 
		default:
		}

		LOG.debug("==> Run - End");
	}

	private void displayNameSheet() {
		LOG.info("Display all sheet name - BEGIN");
		int i = 0;
		Iterator<Sheet> sheets = workbook.iterator();
		while (sheets.hasNext()) {
			final Sheet s = sheets.next();
			System.out.println(++i + " - " + s.getSheetName());
		}
		LOG.info("Display all sheet name - END");
	}

	private void displayConditions() {
		LOG.info("Display conditions - BEGIN");
		System.out.println("***Let choose the condition for copy:");
		for (Map.Entry<String, String> entry : conditions.entrySet()) {
			System.out.println(entry.getKey() + " - " + entry.getValue());
		}
		LOG.info("Display conditions - END");
	}

//	public static void main(String[] args) {
//		Console c = new Console();
//		c.displayConditions();
//	}
}
