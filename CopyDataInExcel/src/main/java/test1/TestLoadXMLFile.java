package test1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.InvalidPropertiesFormatException;

public class TestLoadXMLFile {
	public InputStream inputS = this.getClass().getClassLoader().getResourceAsStream("path of file");

	public static void main(String[] args) {
		Customer customer = new Customer();
		customer.setId(100);
		customer.setName("mkyong");
		customer.setAge(29);

		try {
			InputStream input = TestLoadXMLFile.class.getClassLoader().getResourceAsStream("/Resources/employee.txt");
			File fileOut = new File("/Resources/not-empty-directory.txt");
			if(!fileOut.exists()) {
				fileOut.createNewFile();
			}
			OutputStream output = new FileOutputStream(fileOut);
			int data ;
			while((data = input.read()) != -1 ) {
				output.write(data);
				if(data == -1) {
					System.out.println("success");
				}
			}
			input.close();
			output.close();
//			
//			Properties properties = new Properties();
//			InputStream in = new FileInputStream(
//					"D:\\eclipse-jee-2019-06-R-JUnitTest\\Workspace-JUnit\\CopyDataInExcel\\src\\main\\resources\\Resources\\test1.xml");
//			properties.loadFromXML(in);
//
//			String thach = properties.getProperty("thach.handsome");
//			System.out.println(thach);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
