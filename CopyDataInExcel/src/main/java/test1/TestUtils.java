package test1;

import java.util.StringTokenizer;

import model.Constants;

public class TestUtils {

	public static void main(String[] args) {
		StringBuilder result = new StringBuilder();
		StringBuilder fileName = new StringBuilder();
		String extension;
		final StringTokenizer strToken = new StringTokenizer(".xls", ".");

		fileName.append(strToken.nextToken());
		if (!strToken.hasMoreTokens()) {
			extension = fileName.toString();
			fileName = new StringBuilder();
		} else {
			extension = strToken.nextToken();
			if (strToken.hasMoreElements()) {
				fileName.append(".").append(extension);
				extension = strToken.nextToken(); // not validate what extension?
			}
		}
		fileName.append("(Copy)");
		result.append(fileName).append(Constants.STRING_DOT).append(extension);
		System.out.println(result.toString());

	}

}
